﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    public List<Player> players;
    public List<int> phases;
    public Button buttonStart;
    public Button buttonAsync;
    public bool isAsync;
    private static GameManager m_instance = null;
    private GameManager(){
    }
    public static GameManager getInstance(){
        if (m_instance == null)
        {
            m_instance = new GameManager();
        }
        return m_instance;
    }
    void Awake()
    {
        if (m_instance==null)
        {
            m_instance= this;
        }
        else Destroy(this);
    }
    void Start()
    {
        buttonStart.GetComponent<Button>().onClick.AddListener(TaskOnClickStart);
        buttonAsync.GetComponent<Button>().onClick.AddListener(TaskOnClickStartAsync);
    }

    // Update is called once per frame
    void TaskOnClickStart()
    {
        Debug.Log("Start");
        isAsync=false;
        foreach(Player player in players)
        {
            player.isRunning = true;
        }
    }
    void TaskOnClickStartCoroutine()
    {
        StartCoroutine("RunCoroutine");
    }
    void TaskOnClickStartAsync()
    {
        Debug.Log("Clicked Start Async");
        isAsync=true;
        StartCoroutine("RunCoroutine");
    }
    IEnumerator RunCoroutine()
    {
        for(int i=0;i<phases.Count;i++)
        {
            foreach (Player player in players)
            {
                player.isRunning=true;
            }
            yield return CountPhaseCoroutine(i); //doi den khi tat ca player den phase[i]
        }
    }
    IEnumerator CountPhaseCoroutine(int i)
    {
        int cnt=0;
        while (cnt<3)
        {
            foreach(Player player in players)
            {
                if (player.transform.position.x>=phases[i] && player.isRunning)
                {
                    player.isRunning=false;
                    cnt++;
                }
            }
            yield return null;
        }
    }
    async void RunAsync()
    {
        for(int i=0;i<phases.Count;i++)
        {
            foreach (Player player in players)
            {
                player.isRunning=true;
            }
            await CountPhase(i); //doi den khi tat ca player den phase[i]
        }
    }
    async Task CountPhase(int i)
    {
        int cnt=0;
        while (cnt<3)
        {
            foreach(Player player in players)
            {
                if (player.transform.position.x>=phases[i] && player.isRunning)
                {
                    player.isRunning=false;
                    cnt++;
                }
            }
            await Task.Yield(); 
        }
    }
    
    public void CountPhaseNormal(int i)
    {
        int cnt=0;
        while (cnt<3)
        {
            foreach(Player player in players)
            {
                if (player.transform.position.x>=phases[i] && player.isRunning)
                {
                    player.isRunning=false;
                    cnt++;
                }
            }
        }
    }
}
