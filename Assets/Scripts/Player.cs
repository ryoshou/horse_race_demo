﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

public class Player : MonoBehaviour
{
    public float speed;
    private Rigidbody rb;
    private Vector3 movement;
    public bool isRunning;
    private GameManager gameManager;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        isRunning=false;
        movement = new Vector3(1.0f,0.0f,0.0f);
        gameManager = GameManager.getInstance();
    }
    void Update()
    {
        if (isRunning)
        {
            if (gameManager.isAsync){
                rb.AddForce(movement*speed);
            }
            else
            {
                rb.AddForce(movement*speed);
                Debug.Log(gameManager.phases[0]);
                if (transform.position.x>=gameManager.phases[2])
                {
                    rb.velocity = Vector3.zero;
                }
            }
        }
        else
        {
            rb.velocity = new Vector3(0,0,0);
            rb.AddForce(new Vector3(0,0,0));
        }
    }
}
